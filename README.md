# Clarifai-Powered Image Search

Junhong Chen, jon_chen@outlook.com

## 0 Dependencies

```shell
go get derekparker/trie
```

## 1 How to Build

In `src/`:

```shell
go build -o ../bin/app
```

## 2 How to Run

In root directory:

### a) Fetch Image Tags

```shell
# ./app fetch <API-Key> <Input-Image-URLs-File> <Output-Image-Tag-File>
./bin/app fetch -apiKey=b45762ed4c714a299410247764466aa8 -imageFile=./data/images.txt -tagFile=./data/images.json
```

(*Notice: This API-Key has exceeded the maximum monthly operation limit of Clarifai server. Please try another key.*)

### b) Start Search UI

```shell
# ./app serve <port> <UI-Directory> <Image-Tag-File> <Search-Type>
./bin/app serve -port=8080 -uiDir=./ui -tagFile=./data/images_default.json -searchType=2  # Precise Search
./bin/app serve -port=8080 -uiDir=./ui -tagFile=./data/images_default.json -searchType=1  # Prefix Search
./bin/app serve -port=8080 -uiDir=./ui -tagFile=./data/images_default.json -searchType=0  # Fuzzy Search
```

Then open http://localhost:8080/.

## 3 How to Use

Enter the keyword in the search box and press ENTER.

![screenshot](./screenshot.png)

## 4 Details

1. The images were sent to clarifai backend for analysis in advance(using `fetch.go`). Each image is returned with a list of related tags and corresponding scores. Results are saved in `images.json`.
2. The in-memory data structure used in this project is **trie** with each leaf being a **sorted linked list**. Third-party package `derekparker/trie` is used, which provides fast trie construction and keyword searching.
3. The images are sorted in descending order. When hovering on the image, the score of the image with regard to the keyword is shown. The images are clickable.
4. The UI is built using **Vue.js**. Network connection is required as the web app depends on CDN for the libraries. The images are loaded in the web app. 
5. `server.go` provides a static file server as well as a search API for the app.

### About Fetch

- The API key is encapsulated in a struct called `App` in `app.go`. The method  `Fetch` is its primary duty.
- `Fetch` is provided with the image URLs in the file `images.txt`. 
  1. It separates the URLs and creates multiple requests according to the MAX_IMAGES_PER_REQUEST limit of clarifai server. 
  2. The requests are then sent **asynchronously** with regard to the MAX_REQUESTS_PER_SECOND limit of clarifai server using *goroutine*.
  3. Responses are retrieved and processed from *channel*.

## 5 Inefficiencies

- The trie structure is built upon third-party dependencies without thorough inspection of the source code. This could be solved by further code review.
- The keyword search is not boosted with auto-completion, auto-correction, or fuzzy-query. The `FuzzySearch` API of the `derekparker/trie` library is used for querying the in-memory data structure. Possible improvement, e.g. correcting certain characters, can be made using *Naive Bayes* model or other fancy algorithms.
- The web app does not cache searching history. The images (in *url* form) rendered to the app are fetched and cached by the browser automatically. Possible improvement includes cacheing the search with help from more powerful server API so as to reduce network overhead. But it is only a minor issue in this case.

## 6 Code Structure

- `src/`: Go Program

  - Command-Line Interface:

    - `cli.go`: App Entry.

  - Cacheing the tags of the images in advance:
    - `model.go`: Models for tag request and response
    - `app.go`: Encapsule the http request method into an app. Reference: `Clarifai/clarifai-go`
    - `fetch.go`: Main program to fetch and save the tags using `model.go` and `app.go`.

  - In-memory data structure:
    - `list.go`: Structures and methods relating to sorted linked-list.
    - `storage.go`: Methods for building the in-memory trie structure. Dependence: `derekparker/trie`

  - Server:
    - `server.go`: Static file server for the ui and search api.

    - `test.go`: For search test.

- `ui/`: Web GUI
  - `index.html`
  - `scirpt/app.js`
  - `style/style.css`

- `data/`: Data Files
  - `images.txt`: Provided images library.
  - `images.json`: Cached image tags and scores.
  - `request.json`: Sample request for reference purpose.
  - `response.json`: Sample response for reference purpose.

- `bin`: Executables to be built.

## 7 Modifications

- 2018/06/17:
  - Optimizing `fetch.go` and implementing asynchronous requests.
  - Building executables for `fetch` and `server.`
  - Updating README document.
  - Fixing bugs of search (ERRORS in handling multiple-word search keyword).
- 2018/06/18:
  - Adding `cli.go ` and removing previous executables.