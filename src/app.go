//Reference: https://github.com/Clarifai/clarifai-go/

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

// Configurations
const (
	version = "v2"
	baseURL = "https://api.clarifai.com"
)

// App contains scoped variables for an app
type App struct {
	APIKey string
}

// Helper function to build URLs
func buildURL(endpoint string) string {
	parts := []string{baseURL, version, endpoint}
	return strings.Join(parts, "/")
}

// Helper function to build tag request
func buildTagRequest(urls []string) TagRequest {
	images := make([]TagRequestImage, len(urls))
	for i, url := range urls {
		images[i].Data.Image.URL = url
	}
	return TagRequest{Inputs: images}
}

// NewApp initializes a new Clarifai app
func NewApp(apiKey string) *App {
	return &App{apiKey}
}

// Helper function to make an HTTP request
func (app *App) commonHTTPRequest(jsonBody interface{}, endpoint, verb string, retry bool) ([]byte, error) {
	// Request: Body
	if jsonBody == nil {
		jsonBody = struct{}{}
	}
	body, err := json.Marshal(jsonBody)
	if err != nil {
		return nil, err
	}

	// Request: Creation
	req, err := http.NewRequest(verb, buildURL(endpoint), bytes.NewReader(body))
	if err != nil {
		return nil, err
	}

	// Request: Header
	req.Header.Set("Authorization", "Key "+app.APIKey)
	req.Header.Set("Content-Type", "application/json")

	// Request: Sending
	httpClient := &http.Client{}
	res, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	// Response: Status
	switch res.StatusCode {
	case 200, 201:
		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		return body, err
	case 401:
		return nil, errors.New("AUTHORIZATION_INVALID")
	case 402:
		return nil, errors.New("CLARIFAI_OPERATION_LIMIT_EXCEEDED")
	case 429:
		return nil, errors.New("REQUEST_RATE_LIMIT_EXCEEDED")
	case 400:
		return nil, errors.New("ALL_ERROR")
	case 500:
		return nil, errors.New("CLARIFAI_ERROR")
	default:
		return nil, errors.New("UNEXPECTED_STATUS_CODE: " + strconv.Itoa(res.StatusCode))
	}
}

// Predict allows the app to request tag data for images
func (app *App) Predict(model string, req TagRequest) (*TagResponse, error) {
	if len(req.Inputs) < 1 {
		return nil, errors.New("Requires at least one url")
	}
	res, err := app.commonHTTPRequest(req, "models/"+model+"/outputs", "POST", false)
	if err != nil {
		return nil, err
	}

	tagres := new(TagResponse)
	err = json.Unmarshal(res, tagres)

	return tagres, err
}
