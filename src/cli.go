package main

import (
	"flag"
	"fmt"
	"os"
)

func main() {
	// Constants
	model := "aaa03c23b3724a16a56b629203edc62c"

	// Subcommands
	fetchCommand := flag.NewFlagSet("fetch", flag.ExitOnError)
	serverCommand := flag.NewFlagSet("serve", flag.ExitOnError)

	// Fetch Subcommand Flag Pointers
	apiKeyPtr := fetchCommand.String("apiKey", "eef10f20642e4aceb1f3d2db1d9e364f", "The API Key.")
	inputImageFilePtr := fetchCommand.String("imageFile", "./data/images.txt", "The file that contains image URLs to be processed.")
	outputTagFilePtr := fetchCommand.String("tagFile", "./data/images.json", "The file that contains fetched image tags.")

	// Server Subcommand Flag Points
	portDirPtr := serverCommand.String("port", "8080", "The port to use.")
	uiDirPtr := serverCommand.String("uiDir", "./ui", "The root directory for app UI.")
	tagFilePtr := serverCommand.String("tagFile", "./data/images_default.json", "The fetched image tag file.")
	searchTypePtr := serverCommand.Int("searchType", 0, "fuzzy(0), prefix(1), precise(2)")

	// Check Subcommands
	if len(os.Args) < 2 {
		fmt.Println("`fetch` or `serve` subcommand is required")
		os.Exit(1)
	}

	// Parse Flags
	switch os.Args[1] {
	case "fetch":
		fetchCommand.Parse(os.Args[2:])
	case "serve":
		serverCommand.Parse(os.Args[2:])
	default:
		flag.PrintDefaults()
		os.Exit(1)
	}

	if serverCommand.Parsed() {
		Server(*portDirPtr, *uiDirPtr, *tagFilePtr, int8(*searchTypePtr))
	} else if fetchCommand.Parsed() {
		SaveImageJSON(Fetch(*apiKeyPtr, model, LoadImageURLs(*inputImageFilePtr)), *outputTagFilePtr)
	}
}
