package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"time"
)

const (
	maxImagesPerRequest  = 32
	maxRequestsPerSecond = 6
)

// LoadImageURLs helps to read the image.txt file
func LoadImageURLs(filename string) []string {
	URLs := []string{}
	// Open
	filename, err := filepath.Abs(filename)
	if err != nil {
		panic(err)
	}
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	// Read
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		URLs = append(URLs, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}
	// Return
	return URLs
}

// SaveImageJSON helps to save the images map into file
func SaveImageJSON(images map[string][]Tag, filename string) {
	// Serialize
	jsonData, err := json.Marshal(images)
	if err != nil {
		panic(err)
	}
	// Open
	file, err := filepath.Abs(filename)
	if err != nil {
		panic(err)
	}
	jsonFile, err := os.Create(file)
	if err != nil {
		panic(err)
	}
	defer jsonFile.Close()
	// Save
	jsonFile.Write(jsonData)
	jsonFile.Close()
}

// Fetch asynchronously fetches the tags for the images
func Fetch(apiKey string, model string, URLs []string) map[string][]Tag {
	// Building Requests
	totalURLs := len(URLs)
	requests := []TagRequest{}
	for start := 0; start < totalURLs; start += maxImagesPerRequest {
		end := start + maxImagesPerRequest
		if end > totalURLs {
			end = totalURLs
		}
		urls := URLs[start:end]
		requests = append(requests, buildTagRequest(urls))
	}

	// Utilities
	app := NewApp(apiKey)
	images := make(map[string][]Tag)
	totalRequests := len(requests)
	responses := make(chan *TagResponse, totalRequests)
	throttle := time.Tick(time.Second / maxRequestsPerSecond)

	// Sending Requests
	for i, request := range requests {
		fmt.Printf("Request: %+v / %+v\n", i+1, totalRequests)
		go func(request TagRequest) {
			tagResponse, err := app.Predict(model, request)
			if err != nil {
				panic(err)
			}
			responses <- tagResponse
		}(request)
		<-throttle
	}

	// Handling Responses
	for i := 0; i < totalRequests; i++ {
		tagResponse := <-responses
		for _, imageOutput := range tagResponse.Outputs {
			image := imageOutput.Input.Data.Image.URL
			tags := make([]Tag, len(imageOutput.Data.Concepts))
			for i, tagResult := range imageOutput.Data.Concepts {
				tags[i] = Tag{Name: tagResult.Name, Value: tagResult.Value}
			}
			images[image] = tags
		}
		fmt.Printf("Response: %+v / %+v\n", i+1, totalRequests)
	}

	// Returning Result
	return images
}
