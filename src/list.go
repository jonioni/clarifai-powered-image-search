package main

// ImageNode for sorted linked list
type ImageNode struct {
	Image string
	Value float32
	Next  *ImageNode
}

// Image for exported node
type Image struct {
	URL   string  `json:"url"`
	Score float32 `json:"score"`
}

// Add insert a new node into the sorted linked list
func (head *ImageNode) Add(node ImageNode) {
	current := head
	for current.Next != nil {
		if current.Next.Value > node.Value {
			current = current.Next
		} else {
			break
		}
	}
	node.Next = current.Next
	current.Next = &node
}

// Peek returns the id of the top k images; if k is too large then returns as many as possible
func (head *ImageNode) Peek(k int) []Image {
	current := head.Next
	images := []Image{}
	for current != nil && k > 0 {
		if current.Image != "" {
			images = append(images, Image{URL: current.Image, Score: current.Value})
			k--
		}
		current = current.Next
	}
	return images
}
