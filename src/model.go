//Reference: https://github.com/Clarifai/clarifai-go/

package main

// TagRequest represents the JSON object for request
type TagRequest struct {
	Inputs []TagRequestImage `json:"inputs"`
}

// TagRequestImage represents an image in TagRequest
type TagRequestImage struct {
	Data struct {
		Image struct {
			URL string `json:"url"`
		} `json:"image"`
	} `json:"data"`
}

// TagResponse represents the expected JSON response
type TagResponse struct {
	Status  TagResponseStatus `json:"status"`
	Outputs []TagOutput       `json:"outputs"`
}

// TagResponseStatus represents the status of the TagResponse
type TagResponseStatus struct {
	Code        int    `json:"code"`
	Description string `json:"description"`
}

// TagResponseInfo represents basic info of a tag result
type TagResponseInfo struct {
	ID        string            `json:"id"`
	Status    TagResponseStatus `json:"status"`
	CreatedAt string            `json:"created_at"`
}

// TagResponseModel represents the model in TagResponse
type TagResponseModel struct {
	*TagResponseInfo
	AppID      string `json:"app_id"`
	OutputInfo struct {
		Message string `json:"message"`
		Type    string `json:"type"`
		TypeExt string `json:"type_ext"`
	} `json:"output_info"`
	ModelVersion TagResponseInfo `json:"model_version"`
	DisplayName  string          `json:"display_name"`
}

// TagOutput represents the expected data for a single tag result
type TagOutput struct {
	*TagResponseInfo
	Model TagResponseModel `json:"model"`
	Input struct {
		ID string `json:"id"`
		*TagRequestImage
	} `json:"input"`
	Data struct {
		Concepts []TagResult `json:"concepts"`
	} `json:"data"`
}

// TagResult represents an expected JSON Tag
type TagResult struct {
	ID    string `json:"id"`
	AppID string `json:"app_id"`
	*Tag
}

// Tag represents an expected JSON Tag
type Tag struct {
	Name  string  `json:"name"`
	Value float32 `json:"value"`
}
