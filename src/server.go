package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/derekparker/trie"
)

// storage is used by the server
var storage *trie.Trie

// searchType is specified by the user
var searchType int8

func searchHandler(w http.ResponseWriter, r *http.Request) {
	// Parse Request
	argumentKeyword, ok := r.URL.Query()["keyword"]
	if !ok || len(argumentKeyword) != 1 {
		log.Println("Url Param 'keyword' is missing.")
		return
	}
	keyword := argumentKeyword[0]
	argumentLimit, ok := r.URL.Query()["limit"]
	if !ok || len(argumentLimit) != 1 {
		log.Println("Url Param 'limit' is missing.")
		return
	}
	limit, err := strconv.Atoi(argumentLimit[0])
	if err != nil || limit < 0 {
		log.Println("Url Param 'limit' is mistaken.")
		return
	}
	// Prepare Reponse
	data, err := json.Marshal(Search(storage, keyword, limit, searchType))
	if err != nil {
		fmt.Fprintf(w, "Error: %s", err)
	}
	// Set Response
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

// Server starts the server.
func Server(port string, uiDir string, tagFile string, searchCode int8) {
	// Storage
	storage = Build(Load(tagFile))
	searchType = searchCode
	// Server
	http.Handle("/", http.FileServer(http.Dir(uiDir)))
	http.HandleFunc("/search", searchHandler)
	log.Println("Server Start")
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
	log.Println("Server End")
}
