package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"

	"github.com/derekparker/trie"
)

// Load helps to load the json data in file into map
func Load(filename string) map[string][]Tag {
	// Open
	file, err := filepath.Abs(filename)
	if err != nil {
		panic(err)
	}
	raw, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}
	// Load
	var images map[string][]Tag
	json.Unmarshal(raw, &images)
	// Return
	return images
}

// Helper function to tidy keyword with space
func tidy(keyword string) string {
	return strings.Join(strings.Fields(keyword), "_")
}

// Build builds the trie structure
func Build(images map[string][]Tag) *trie.Trie {
	var head *ImageNode
	storage := trie.New()
	for image, tags := range images {
		for _, tag := range tags {
			head = getDefault(storage, tag.Name)
			head.Add(ImageNode{Image: image, Value: tag.Value})
		}
	}
	return storage
}

// getDefault gets the list by key; if key doesn't exist then creates a default list
func getDefault(storage *trie.Trie, key string) *ImageNode {
	key = tidy(key)
	var head *ImageNode
	node, ok := storage.Find(key)
	if !ok {
		head = &ImageNode{Image: "", Value: 0, Next: nil}
		storage.Add(key, head)
	} else {
		head = node.Meta().(*ImageNode)
	}
	return head
}

// get returns as many tags of the key as possible within size
func get(storage *trie.Trie, key string, size int) []Image {
	key = tidy(key)
	testNode, _ := storage.Find(key)
	return testNode.Meta().(*ImageNode).Peek(size)
}

// remove removes the key from keys.
func remove(keys []string, key string) []string {
	for i, k := range keys {
		if k == key {
			return append(keys[:i], keys[i+1:]...)
		}
	}
	return keys
}

// Search finds as many images of the key as possible within the size
func Search(storage *trie.Trie, keyword string, size int, searchType int8) []Image {
	keyword = tidy(keyword)
	remaining := size
	images := []Image{}
	encountered := map[string]bool{}

	var keys []string
	switch searchType {
	case 1: // Prefix Search
		keys = storage.PrefixSearch(keyword)
		_, ok := storage.Find(keyword)
		if ok {
			keys = append([]string{keyword}, remove(keys, keyword)...)
		}
		log.Printf(`Prefix Search: "%+v", Possible Keys: %+v`, keyword, keys)
	case 2: // Precise Search
		keys = []string{}
		_, ok := storage.Find(keyword)
		if ok {
			keys = []string{keyword}
		}
		log.Printf(`Precise Search: "%+v", Possible Keys: %+v`, keyword, keys)
	default: // Fuzzy Search
		keys = storage.FuzzySearch(keyword)
		log.Printf(`Fuzzy Search: "%+v", Possible Keys: %+v`, keyword, keys)
	}

	for _, key := range keys {
		if remaining <= 0 {
			break
		}
		imagesOfKey := get(storage, key, size)
		for _, image := range imagesOfKey {
			if remaining <= 0 {
				break
			}
			if encountered[image.URL] == true {
				// pass
			} else {
				encountered[image.URL] = true
				images = append(images, image)
				remaining--
			}
		}
	}
	return images
}
