package main

import (
	"fmt"
)

func test1(tagFile string) {
	storage := Build(Load(tagFile))
	fmt.Printf("Test: Dog\nResult: %+v\n", Search(storage, "dog", 10, 2))
}

func test2(tagFile string) {
	images := Load(tagFile)
	// Naive Database
	checklist := map[string](map[string]bool){} // { Tag: [Image.URLs], }
	database := map[string][]string{}
	var tidyTag string
	for image, tags := range images {
		for _, tag := range tags {
			tidyTag = tidy(tag.Name)
			if checklist[tidyTag] == nil {
				checklist[tidyTag] = map[string]bool{}
			}
			if checklist[tidyTag][image] == true {

			} else {
				checklist[tidyTag][image] = true
				database[tidyTag] = append(database[tidyTag], image)
			}
		}
	}
	// Build In-Memory Trie Storage
	var total int
	var returnedImages []Image
	storage := Build(images)
	for tagName, imageURLs := range database {
		total = len(imageURLs)
		// fmt.Println(tagName, total)
		returnedImages = Search(storage, tagName, total, 2)
		if len(returnedImages) != total {
			fmt.Printf("Error: %+v, not all image is returned.\n", tagName)
		}
		var previousScore float32
		for i, returnedImage := range returnedImages {
			if i > 0 && returnedImage.Score > previousScore {
				fmt.Println("Wrong Order!", tagName)
			}
			previousScore = returnedImage.Score
			if checklist[tagName][returnedImage.URL] == true {

			} else {
				fmt.Printf("Error: %+v, returned %+v is wrong.\n", tagName, returnedImage.URL)
			}
		}
	}
}

func mainTest() {
	tagFile := "../data/images_default.json"
	// test1(tagFile)
	test2(tagFile)
}
