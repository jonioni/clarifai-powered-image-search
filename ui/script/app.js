var app = new Vue({
    el: '#app',
    data: {
        feedback: "",
        keyword: '',
        limit: 10,
        images: []
    },
    methods: {
        search (event) {
            if (this.keyword) {
                fetch(`/search?keyword=${this.keyword.toLowerCase()}&limit=${this.limit}`)
                .then(response => response.json())
                .then(data => {
                    this.images = data
                    this.feedback = data.length ? "" : "Nothing Found 😰"
                })
            }
            document.activeElement.blur()
        }
    }
})